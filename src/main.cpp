#include <iostream>
#include <opencv2/opencv.hpp>
#include <SFML/Window.hpp>

using namespace cv;

std::vector<KeyPoint> findBlobs(Mat& input, int threshold_minimum) {
    Mat threshold_output;
    std::vector<std::vector<Point> > contours;
    std::vector<Vec4i> hierarchy;

    /// Detect edges using Threshold
    threshold(input, threshold_output, threshold_minimum, 255, THRESH_BINARY);

    /// Find contours
    findContours(threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

    /// Approximate contours to polygons + get bounding rects and circles
    std::vector<std::vector<Point> > contours_poly(contours.size());
    std::vector<Rect> boundRect(contours.size());
    std::vector<Point2f> center(contours.size());
    std::vector<float> radius(contours.size());

    std::vector<KeyPoint> r;
    for(unsigned int i = 0; i < contours.size(); i++) {
        approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
        boundRect[i] = boundingRect(Mat(contours_poly[i]));
        minEnclosingCircle(contours_poly[i], center[i], radius[i]);
        r.push_back(KeyPoint(center[i], radius[i]));
    }
    return r;
}

bool blobSort(const KeyPoint& a, const KeyPoint& b) {
    return a.size > b.size;
}

Point2f mapPoint(Point2f src, Size frame_size) {
    src.x /= frame_size.width;
    src.y /= frame_size.height;
    src.x = 1 - src.x;
    float sens = 2;
    src.x = 0.5 - sens * (0.5 - src.x);
    src.y = 0.5 - sens * (0.5 - src.y);
    return src;
}

void setMouse(Point2f point) {
    // set mouse position
    sf::Mouse::SetPosition(sf::Vector2i((int)round(point.x * sf::VideoMode::GetDesktopMode().Width),
                                        (int)round(point.y * sf::VideoMode::GetDesktopMode().Height)));
}

int main() {
    VideoCapture cam(0);
    if(!cam.isOpened()) {
        std::cerr << "The camera could not be opened/found." << std::endl;
        return 1;
    }

    namedWindow("video",1);

    while(true) {
        // Record the frame
        Mat frame;
        cam >> frame;
        Mat frame_gray;
        cvtColor(frame, frame_gray, CV_BGR2GRAY);

        std::vector<KeyPoint> blobs = findBlobs(frame_gray, 200);

        std::sort(blobs.begin(), blobs.end(), blobSort);

        for(unsigned int i = 0; i < blobs.size(); ++i) {
            circle(frame, blobs[i].pt, blobs[i].size, cvScalar(0, 0, 255));
        }

        if(blobs.size() > 0) {
            setMouse(mapPoint(blobs[0].pt, frame.size()));
        }

        imshow("video", frame);
        if(waitKey(30) >= 0) break;
    }

    return 0;
}
